package main

import (
	"fmt"
	"net/http"
	"text/template"
)

type User struct {
	Name      string
	Age       uint16
	Balance   int16
	AvgGrades float64
	Happiness float64
	Hobbies   []string
}

func homePage(w http.ResponseWriter, r *http.Request) {
	bob := User{"John", 19, -340, 5.98, 67.7, []string{"Football", "Baseball", "Tennis"}}
	tmpl, _ := template.ParseFiles("templates/index.html")
	tmpl.Execute(w, bob)
	//bob.setNewName("Mark")
	//fmt.Fprintf(w, bob.getAllInfo())
}

func (u *User) getAllInfo() string {
	return fmt.Sprintf("User's name is %s. He is %d years old. Has balance is %d", u.Name, u.Age, u.Balance)
}

func (u *User) setNewName(newName string) {
	u.Name = newName
}

func categoriesPage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Categories are available")
}

func handleRequest() {
	http.HandleFunc("/", homePage)
	http.HandleFunc("/categories/", categoriesPage)
	http.ListenAndServe(":9090", nil)
}
func main() {
	handleRequest()
}
